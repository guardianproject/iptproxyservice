
# IPtProxyService

IPtProxyServices is a free proxy service for Android phones that empowers other apps to use the internet more freely. It acts as a simple Pluggable Transport background service for apps to hide their Internet traffic by tunneling it through another protocol, such as Obfs4, Snowflake or DNSTT. 

The nickname for this work is "Actually Pluggable Transports" or "PLUGGABLE Pluggable Transports" since this allows pluggable transports to be plugged in at runtime, and empowers users to install different pluggable transports as services on their Android devices, as they are available.

## Building

`./gradlew assemble` is the quickest way to get started, that builds a universal
APK.  It is also possible to build per-ABI APKs:

```bash
./gradlew assembleRelease -Pabi-splits               # build all variants
./gradlew assembleRelease -Pabi-splits=arm64-v8a     # build one
./gradlew assembleRelease -Pabi-splits=armeabi-v7a,arm64-v8a  # multiple

# or set it in the properties
echo abi-splits=true >> gradle.properties
```

# Learn more about PLUGGABLE Pluggable Transports

We've had this idea that you should be able to have circumvention techniques on a mobile device that can be dynamically updated, upgraded and changed at runtime so that a user could plug in a new transport based on what their need is. Now the way this has been solved, mostly with Tor Browser, Orbot, Onion Browser, Leap and others apps is to just bundle a bunch of transports into the app, and then upgrade as you need new ones. That is compile or build-time pluggability, and it works for the most part.

There is a standard interface that can be used by developers to build to and we can ship a new transport without too much pain. However, with this work, we wanted to explore the idea of runtime plugability by way of how the Android operating system works. We sought to build upon the idea that you can have different services running on the device installed that are invisible, background services, things that users don't necessarily open as an app. These can be discovered by another app or another service at runtime and then utilized to connect in the way the pluggable transports enabled.  This is how Google Play Services work where you detect if it is available. If so, then you use XYZ service. We've been exploring that for a while for a year and that we have running code to share as we're wrapping up this project and it works pretty well.

All of this is first built on our TorServices project, creating a new developer focused service for apps that want to integrate with Tor as opposed to using the full Orbot app. Orbot is increasingly an end-user VPN, while TorServices is targeted at developers. TorServices is one kind of service that a developer can discover in their app and say "if there's Tor, I want to use it".  I'm going to ask it to start, here's what I do. We have been shipping that capability for a while in production, and as an extension of that, we wanted to add the capability for pluggable transports to be used. We have built upon our IPtProxy library to develop the pluggable transport services and this exposes in the Android parlance, an "Intent Action".

The intent allows the service to describe itself and what it offers. It offers a plugable transport start and status intent action that other apps can call. You can also access information about the type of pluggable transport so that a developer can know if they need to send an initialization string when they start it. If you launch this as a service, you have to call it as a foreground service, though you can bind to it directly in-process, as well. Once is is running, it will reply with the status of the transport that it's running, the port it's available at, and so on. This is all using the interprocess-based use of a pluggable transport via socks port. It's the works very well in this environment. Now if we have the binding support, we actually bind to the service then a it doesn't need to run as its own foreground service which is great. Then it can actually be used in a pluggable transport 2.0 or beyond with a direct interface say in the Java column language.

So you bind to it then you get the PT in your face there. But for now we're doing PT1 with socks because it's quite easy. You interrogate is there PT on this device? If so is it one that I know? Then, start it up, respond to the status and then you can figure out how to use it. Tt can work with tor or it can work with any app that wants to use one of these pluggable transports.

That is pretty exciting, that we can have this shared service, offering Snowflake or OBFS4 to many apps that need them. The current IPtProxyService is built on IPtProxy library, which is the primary mobile library for Android and iOS access to pluggable transports such as Snowflake, Obfs4, Meek and DNSTT. All of these are packaged in this one plugin, and it can be share dby apps that need to use it. Pluggable Transports are not providing anonymity, they are not providing security, they are providing traffic obfuscation. They offer a tunnel that is free to use, and any app can tunnel traffic through it to a pluggable transport endpoint server that allows that kind of traffic. It is all working as we envisioned and we will continue to explore this approach in the future with other grants and projects that can benefit from this.

Here is that you don't have to add that bulk to your app. So of these transports. So with orbot, we've gone up been on the kind of continued mission to make the smallest possible version of Orbot. We can and not everyone needs and pluggable transports or you know bridges and if they don't need them then you know maybe they shouldn't have to download them in the app and when they do need them then it's just a plugin.  The idea of plugins on Android is well established. If you have Google Play or F-Droid, it is easy to ask the user to install a plugin app. We can ship Orbot "MAX" with all the PT's integrated, but now we can also ship a slim or "mini" version that works with IPtProxyService plugins. It also means at runtime, someone could implement a new transport, package it in IPtProxyService, and provide the user with a configuration string to use it. If an app that is IPtProxyService aware is given that configuration, and the IPtProxyService APK plugin is installed for that type, then at runtime, they can all be linked together into a usable solution for a real person.

