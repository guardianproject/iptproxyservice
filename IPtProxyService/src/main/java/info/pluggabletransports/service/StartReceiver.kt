package info.pluggabletransports.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import info.pluggabletransports.service.IPtProxyService

/**
 * Processes [Intent]s from apps that are requesting PT proxying,
 * including replying to the apps when the user has disabled automatic
 * starts.  This also sanitizes incoming `Intent`s before
 * forwarding it to [IPtProxyService].
 */
class StartReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        if (TextUtils.equals(action, IPtProxyService.ACTION_START)) {
            val packageName = intent.getStringExtra(IPtProxyService.EXTRA_PACKAGE_NAME)
            val startPTIntent = Intent(context, IPtProxyService::class.java)
            startPTIntent.action = action
            if (packageName != null) {
                startPTIntent.putExtra(IPtProxyService.EXTRA_PACKAGE_NAME, packageName)
            }
            if (App.useForeground(context)) {
                ContextCompat.startForegroundService(context, startPTIntent)
            } else {
                context.startService(startPTIntent)
            }
        }
    }

    companion object {
        const val TAG = "StartReceiver"
        fun start(context: Context?) {
            val intent = Intent(IPtProxyService.ACTION_START)
            intent.setClass(context!!, StartReceiver::class.java)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
    }
}